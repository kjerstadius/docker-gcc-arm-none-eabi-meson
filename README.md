gcc-arm-none-eabi-meson
=======================

[![pipeline status](https://gitlab.com/kjerstadius/docker-gcc-arm-none-eabi-meson/badges/master/pipeline.svg)](https://gitlab.com/kjerstadius/docker-gcc-arm-none-eabi-meson/commits/master)

*NB: This image is currently based on Alpine Linux edge and uses packages from
the testing repository. As such, stability cannot be 100% guaranteed.*

This Docker image contains the GCC-based toolchain for compiling bare-metal
firmware for ARM architectures such as the Cortex M4, used in for example the
STM32F4 series of chips. In addition to the compiler toolchain, this image also
bundles [ninja] and [Meson], meaning it should contain everything necessary to build
firmware for embedded ARM devices using the Meson build system. [Gcovr] is also
included so that the Meson coverage targets can be used.

[ninja]: https://ninja-build.org/
[Meson]: https://mesonbuild.com
[Gcovr]: https://gcovr.github.io/

Usage
-----
To use this image in the context of a Meson build, prepend your Meson or ninja
calls with the following:
~~~
docker run --rm -t -v "$PWD":/tmp/build -w /tmp/build gcc-arm-none-eabi-meson COMMAND
~~~

Of course it's also possible to call the various parts of the toolchain
directly, for example:
~~~
docker run --rm -t -v "$PWD":/tmp/build -w /tmp/build gcc-arm-none-eabi-meson arm-none-eabi-gcc
~~~

Note: The `-t` flag prompts Docker to *"Allocate a pseudo-tty"*. This flag is
added in order to preserve niceties such as color output in the terminal.
However, it isn't really required in order for the image to function as
intended.
